import axios from 'axios';

const createAxios = () => axios.create({
  baseURL: '',
  timeout: 20000,
  headers: {
    Accept: 'application/json',
  },
});

const axiosInstance = createAxios();

export default axiosInstance;
