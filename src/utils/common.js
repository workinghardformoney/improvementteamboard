import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();

export const redirectTo = (path) => history.push(path);

export const importAll = (r, hasDefault) => r.keys().map((e) => {
  const path = e.split('/');
  return {
    name: path
      .slice(-1)[0]
      .split('.')[0]
      .toUpperCase(),
    key: path.reverse()[1],
    data: hasDefault ? r(e).default : r(e),
  };
});

export const createEmptyMatrix = (x = 100, y = 100) => {
  const m = [];
  while (m.push(new Array(y)) < x);
  return m;
};

export default this;
