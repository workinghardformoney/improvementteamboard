/* eslint-disable no-console */
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from './store';

import App from './pages/App';

const store = configureStore();

const rootNode = document.getElementById('root');

console.log(
  '%c%s',
  'color: green; background: black; font-size: 14px;',
  `Running on ${process.env.NODE_ENV.toUpperCase()} environment`,
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootNode,
);
