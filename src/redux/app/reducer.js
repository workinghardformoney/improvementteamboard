import * as types from './types';

const INITIAL_STATE = {
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  if (action.type.includes('__FETCHING')) return { ...state, loading: true };
  if (action.type.includes('__FAIL')) return { ...state, loading: false };
  switch (action.type) {
    case types.INITIAL:
      return { ...state };
    default:
      return state;
  }
};
