import { put, takeLatest } from 'redux-saga/effects';
import * as types from './types';

export function* getAllNecessaryDataSaga() {
  yield takeLatest(types.INITIAL, function* () {});
}
