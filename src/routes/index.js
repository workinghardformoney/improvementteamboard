import { Route, Switch, Redirect } from 'react-router-dom';

import Pages from '../pages';

const { Home, Landing, Login } = Pages;

const routers = [
  { path: '/', component: Landing, hasAuth: false },
  { path: '/login', component: Login, hasAuth: false },
  { path: '/home', component: Home, hasAuth: true },
];

const PublicRoutes = () => (
  <Switch>
    {routers
      .filter((r) => !r.hasAuth)
      .map((r) => (
        <Route key={`public-${r.path}`} exact path={r.path} component={r.component} />
      ))}
    <Redirect to="/login" />
  </Switch>
);

const AuthRouters = () => (
  <Switch>
    {routers
      .filter((r) => r.hasAuth)
      .map((r) => (
        <Route key={`auth-${r.path}`} exact path={r.path} component={r.component} />
      ))}
    <Redirect to="/" />
  </Switch>
);

const AllRouters = () => (
  <Switch>
    {routers.map((r) => (
      <Route key={`all-${r.path}`} exact path={r.path} component={r.component} />
    ))}
    <Redirect to="/" />
  </Switch>
);

export { PublicRoutes, AuthRouters, AllRouters };
