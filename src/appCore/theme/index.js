import * as colors from './colors';
import * as styles from './styles';

const fonts = {
  default: 'Noto Serif',
};

export default {
  styles,
  colors,
  fonts,
};
