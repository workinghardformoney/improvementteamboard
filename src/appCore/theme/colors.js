export const primary = '#151938';
export const secondary = '#66ba45';
export const tertiary = '#295549';
export const quaternary = '#e9ecdb';
