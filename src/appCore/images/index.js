/* eslint-disable no-console */

import { generatePathsOfImages } from '../generators';

const importImages = () => {
  const importObject = generatePathsOfImages();
  console.log(
    '%c%s',
    'color: green; background: black; font-size: 14px;',
    'IMAGES PATHS:',
    importObject,
  );
  return importObject;
};

export default importImages();
