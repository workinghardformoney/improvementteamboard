const importAll = (r) => r.keys().map((e) => ({
  name: e
    .split('/')
    .slice(-1)[0]
    .split('.')[0]
    .toUpperCase(),
  data: r(e),
}));

export const generatePathsOfImages = () => {
  try {
    const photos = importAll(require.context('./images', true, /\.(png|jpe?g|svg)$/i));
    return photos.reduce((p, c) => ({ ...p, [c.name]: c.data }), {});
  } catch (err) {
    return {};
  }
};
