import { Helmet, HelmetProvider } from 'react-helmet-async';
import { Router } from 'react-router-dom';

import { PublicRoutes, AuthRouters, AllRouters } from '../../routes';
import { images, theme } from '../../appCore';
import { APP_NAME } from '../../utils/constants';
import { history } from '../../utils';

export default () => (
  <HelmetProvider>
    <Helmet titleTemplate={APP_NAME} defaultTitle={APP_NAME}>
      <meta name="description" content="description" />
    </Helmet>
    <Router history={history}>
      <AllRouters />
    </Router>
  </HelmetProvider>
);
