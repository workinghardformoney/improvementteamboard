/* eslint-disable react/prop-types */
// import { MultiGrid, Grid } from 'react-virtualized';
import Grid from 'react-virtualized/dist/commonjs/Grid';

import { createUseStyles } from 'react-jss';
import 'react-virtualized/styles.css';

import { createEmptyMatrix } from '../../utils';

import useControl from './useControl';

const useStyles = createUseStyles({
  grid: {
    width: '80vh',
    height: '80vh',
    margin: 'auto',
    // overflow: 'auto',
    border: '1px solid gray',
    position: 'relative',
  },
  cell: {
    width: '50px',
    height: '50px',
    background: 'yellow',
    outline: 'green',
  },
  gridVirtual: {
    border: '1px solid',
  },
});

const Cell = () => {
  const classes = useStyles();
  return <div className={classes.cell}>a</div>;
};

export default () => {
  const classes = useStyles();
  const INITIALSTATE = {
    loading: false,
    list: [
      [1, 2, 3, 4, 5, 6],
      [1, 2, 3, 4, 5, 6],
      [1, 2, 3, 4, 5, 6],
    ],
  };
  const {
    state, setState, onAppend, onLayoutComplete,
  } = useControl(INITIALSTATE);

  const cellRenderer = ({
    columnIndex, key, rowIndex, style,
  }) => (
    <Cell key={key} style={style}>
      {state.list[rowIndex][columnIndex]}
    </Cell>
  );

  return (
    <div className={classes.grid}>
      <Grid
        className={classes.gridVirtual}
        cellRenderer={cellRenderer}
        columnCount={state.list[0].length}
        columnWidth={50}
        height={500}
        rowCount={state.list.length}
        rowHeight={50}
        width={500}
        // autoContainerWidth
        autoHeight
        autoWidth
      />
    </div>
  );
};
