import { useState, useEffect } from 'react';

export default (initialState, props) => {
  const [state, _setState] = useState(initialState);
  const setState = (obj) => _setState({ ...state, ...obj });

  useEffect(() => () => {}, []);

  return {
    state,
    setState,
  };
};
