import { importAll } from '../utils';

const pagesSource = importAll(require.context('./', true, /index.js$/i), true);

const Pages = pagesSource.filter((p) => p.key !== '.' && p.data).reduce((p, c) => ({ ...p, [c.key]: c.data }), {});

export default Pages;
