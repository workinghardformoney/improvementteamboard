const path = require('path');

const merge = require('webpack-merge');
const common = require('./webpack.common.js');

const clientPort = process.env.PORT || 12345;

module.exports = merge(common, {
  mode: 'development',
  devServer: {
    port: clientPort,
    historyApiFallback: true,
    disableHostCheck: true,
    hot: true,
  },
});
